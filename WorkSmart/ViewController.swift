//
//  ViewController.swift
//  WorkSmart
//
//  Created by kys on 2017. 7. 27..
//  Copyright © 2017년 skmns. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        let urlString:URL = URL(string: "http://61.104.71.225:8180/conferenceRoom")!
        //let urlString:URL = URL(string: "http://10.125.203.47:8080/conferenceRoom")!
        let request = URLRequest(url: urlString)
        webView.loadRequest(request)
        webView.scrollView.bounces = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

